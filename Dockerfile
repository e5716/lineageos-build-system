FROM archlinux:latest

LABEL maintainer="y45"
LABEL projectUrl="https://gitlab.com/e5716"

ARG BUILD_USER=los

# Creating layers for the image: we need to start from the less volatile according to https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#use-multi-stage-builds

USER root
RUN useradd -m -G nobody $BUILD_USER \ 
    && echo -e "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist\n" >> /etc/pacman.conf \
    && pacman -Syu --noconfirm base-devel git \
    && echo "$BUILD_USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

WORKDIR /home/$BUILD_USER
USER $BUILD_USER
RUN git clone https://aur.archlinux.org/yay-bin.git \
    && cd yay-bin \
    && makepkg -s --skippgpcheck \
    && find . -iname "*.pkg.tar.zst" -exec sudo pacman --noconfirm -U {} \; \
    && cd ..

# From https://wiki.archlinux.org/title/Android#Building
RUN yay -Syu --noconfirm --answerclean None --answerdiff None \
    multilib-devel \
    lineageos-devel \
    libxcrypt-compat \
    ttf-dejavu \
    trousers-fedora-patches \
    vboot-utils \
    maven \
    gradle \
    openssh \
    time

COPY utils/ /home/$BUILD_USER/

# Environment variables should be near the end of the Dockerfile to avoid recompilation of upper layers
# Available buildtypes: https://source.android.com/setup/build/building#lunch
# Note: building for an emulator will only work for x86 AND "eng" type. Trying other combinations might lead to weird and non-descriptive errors when building.
ENV DEVICE_TARGET "lineage_sdk_phone_x86-eng"
ENV BUILD_DIRECTORY "/home/${BUILD_USER}/lineageos"
ENV LOS_BRANCH "lineage-19.1"
ENV GIT_USERNAME "LineageOS Docker"
ENV GIT_EMAIL "lineageos@y45.sanmarcos"
ENV USE_CCACHE "1"
ENV CCACHE_SIZE "25G"
ENV CCACHE_DIR "$BUILD_DIRECTORY/ccache"
ENV CCACHE_EXEC "/usr/bin/ccache"
#ENV CCACHE_COMPRESS "false"

USER $BUILD_USER
VOLUME $BUILD_DIRECTORY

ENTRYPOINT bash ./run.sh
