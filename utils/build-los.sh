#!/bin/bash

# Based on https://wiki.lineageos.org/emulator

function printMessage
{
    NOW=$(date +"%Y-%m-%d %T")
    echo "[$NOW] ${1}"
}
function finishError
{
    printMessage "Error: ${1}"
    exit 1
}
function welcome
{
    printMessage "Running LOS Build System with the following parameters:"
    echo -e "\tDevice: ${DEVICE_TARGET}"
    echo -e "\tUse ccache?: ${USE_CCACHE}"
    echo -e "\tDir ccache: ${CCACHE_DIR}"
    echo -e "\tUse compressed ccache?: ${CCACHE_COMPRESS}"
}
function syncSources
{
    printMessage "Syncing sources"

    git config --global user.name "${GIT_USERNAME}"
    git config --global user.name "${GIT_EMAIL}"

    if [ ! -d "./.repo" ]
    then
        printMessage "Init git-repo in $(pwd)"
        repo init -u https://github.com/LineageOS/android.git -b "${LOS_BRANCH}" || finishError "Could not init repo"
    fi

    repo sync --verbose || finishError "Could not sync source files"
}

function build
{
    printMessage "Starting LOS build"

    source build/envsetup.sh || finishError "Error when sourcing envsetup.sh"
    lunch $DEVICE_TARGET || finishError "Error at lunch"
    # Don't forget to have a big swapfile enabled. For a lower memory usage, run "mka -j2" or any other number
    mka || finishError "Error at building the image with mka"
    mka sdk_addon || finishError "Error when generating the emulator image for AVD"
}
function init
{
    cd "${BUILD_DIRECTORY}" || finishError "Error when cd to main build folder"
    sudo chmod 777 .

    welcome
    ccache -M $CCACHE_SIZE || finishError "Could not setup ccache size"
    syncSources
    build
    printMessage "Task finished"

    exit
}

init

# "Que chingue a su madre Gooooolag...¡y que la vuelva a chingar!" 
