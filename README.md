# LineageOS Build System

This is an attempt to create a basic build system for LineageOS images based on Arch Linux.

[[_TOC_]]

## Getting started

Currently this build system is only focused to create emulator images of LineageOS (x86) based on LineageOS 19.1. Feel free to customize it for building real device images of LineageOS or derivated custom ROMs.

TL;DR: it follows the steps to create an emulator image [from the LineageOS Wiki](https://wiki.lineageos.org/emulator), but using an Arch Docker container instead of Ubuntu.

## A bit of history

This small project is strongly based on a previous build system created a couple of years ago to build [LineageOS with MicroG](https://gitlab.com/e5716/archive/minimal-docker-for-lineageos) which also runs the whole building operation on the cloud.

You can take a look to that archived repository if you want to run this new and simplified build system on the cloud.

For simplicity, I just ran the whole operation on my local machine.

## Requirements

I ran the build operation on this hardware:

- AMD Ryzen 5 2600
- 16GB RAM + 16 GB of a swap file
- Network download speed of 100 Mbps

Estimated times and sizes (at the moment I'm writing this)

- Sources download: around 70GB downloaded in around 3-4 hours
- Files on disk: around 250GB, including the downloaded sources, files created during the build time and the ccache files
- Compilation time: 3-5 hours on my hardware, but this is not exact. My build failed several times and some items were already reused from the ccache.

### Tips to avoid errors

1. The most important: make sure you have at least 16GB of RAM AND a big swap (I like the swapfile) on the host machine. You don't need to create the swapfile inside the Docker container. In any other case, the building will fail because it will run out of memory and weird errors will start to appear.
2. Build ONLY for x86 emulator AND the "eng" variant. Any other emulator combinations (like "user" variant or choosing the x86_64 architecture) apparently are not tested and they throw errors difficult to understand. Also, it looks like the arm architecture is not supported for emulators (please refer to [Censorddit /r/LineageOS](https://old.reddit.com/r/LineageOS/) for further information)

## Setup

You don't need to modify the environment variables if you're building for an x86 emulator. If you are building for another device, you might like to add additional instructions to the build script.

## Running build

Running the build should be as easy as execute a couple of commands.

1. Build the image: `docker build --network=host -t y45/lineageos-build-system https://gitlab.com/e5716/lineageos-build-system/image.git`

2. Run a new container that will build your LOS image automatically: `sudo docker run -v /path/to/your/build/folder:/home/los/lineageos --network=host -t y45/lineageos-build-system`

3. When finished, the emulator image ready to use with AVD should be under `/path/to/your/build/folder/out/host/linux-x86/sdk_addon/*.zip`

Of course, the `/path/to/your/build/folder` it's and absolute path to a folder inside your host system (NOT the Docker container filesystem). Make sure it has enough space to store everything mentioned before.

>Note: use the host network for a better performance on network operations, instead of bridged connections. [Source](https://docs.docker.com/engine/reference/run/#network-host)

### Extra

If using AVD directly without Android Studio under Arch Linux (host), copy the generated image to `/opt/android-sdk/system-images/android-31/lineageos/x86/` folder. Then, you can create new emulator instances from that image.

## Credits

Thanks to

- **LineageOS community** for this awesome ROM
- [**lineageos-devel**](https://aur.archlinux.org/packages/lineageos-devel) submitters and maintainers of the AUR metapackage
- [**android-devel**](https://aur.archlinux.org/packages/lineageos-devel) submitters and maintainers of the AUR metapackage
- Everyone mentioned [on the the old project Credits section](https://gitlab.com/e5716/archive/minimal-docker-for-lineageos)
- Future contributors of this project

## License

GNU GPL 3.0